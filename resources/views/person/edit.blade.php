@extends("layouts.app")

@section("content")
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <form method="POST"  action="{{URL::route('actualizar')}}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Nombre</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{$person->name}}">
                </div>

                <div class="form-group">
                    <label for="">Ciudad: {{$citySelected->name}}</label>
                    <select class="form-control" name="city" id="">
                        <option >Elegí una ciudad</option>
                        @foreach($cities as $city)
                        <option value="{{$city->id}}" >{{$city->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="name">Carrera</label>
                    @if($person->career==1)
                    <input type="checkbox" checked="checked" name="career" value="">
                    @else
                    <input type="checkbox" name="career" value="">
                    @endif
                </div>

                <div class="form-group">
                    <label for="">Sexo</label>
                    @if($person->sex == 'M')
                    <div class="radio">
                        <label><input type="radio"checked="checked" name="sex" value="M">Masculino</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" name="sex" value="F">Femenino</label>
                    </div>
                    @else
                    <div class="radio">
                        <label><input type="radio" name="sex" value="M">Masculino</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" checked="checked" name="sex" value="F">Femenino</label>
                    </div>
                    @endif
                </div>

                <input type="hidden" name="person_id" value="{{$person->id}}">
                <button class="btn btn-info" type="submit">Enviar</button>
            </form>
        </div>
    </div>
</div>
@endsection