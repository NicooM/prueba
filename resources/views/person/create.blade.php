@extends("layouts.app")

@section("content")

<div class="container">
    <div class="col-md-12">

        <a href="{{ URL::route('lista') }} ">Listado de personas</a>

        <a href="{{ URL::route('inicio') }} ">Volver</a>



        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif


        <form method="POST" id="form"  action="{{ url('/insert') }}">

            {{ csrf_field() }}

            <div class="form-group">
                <label for="name">Nombre</label>
                <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                <span id="error-name" class="help-block"></span>
            </div>

            <div class="form-group">

                <label for="">Ciudad</label>
                <select class="form-control" name="city_id" id="city_id">
                    <option value="" >Elegí una ciudad</option>
                    @foreach($cities as $city)
                    <option value="{{$city->id}}" >{{$city->name}}</option>
                    @endforeach
                </select>
                <span id="error-city" class="help-block"></span>



            </div>

            <div class="form-group">
                <label for="name">Carrera</label>
                <input type="checkbox" name="career" id="career">
            </div>

            <div class="form-group">
                <label for="">Sexo</label>
                <div class="radio">
                    <label><input type="radio" name="sex" id="sex" value="M">Masculino</label>
                </div>
                <div class="radio">
                    <label><input type="radio" name="sex" id="sex" value="F">Femenino</label>
                </div>
                <span id="error-sex" class="help-block"></span>


            </div>

            <button class="btn btn-info" type="submit" onclick="addPerson()" >Enviar</button>

        </form>





    </div>
</div>


@endsection