@extends("layouts.app")

@section("content")
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <p>Lista de personas</p>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Ciudad</th>
                        <th>Carrera</th>
                        <th>Sexo</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($people as $person)
                    <tr>
                        <td>{{$person->personName}}</td>
                        <td>{{$person->cityName}}</td>
                        <td>
                            @if($person->career==0)
                            No
                            @else
                            Si
                            @endif
                        </td>
                        <td>{{$person->sex}}</td>
                        <td>
                            <a href="{{URL::route('editar', $person->id)}} ">Editar</a>
                        </td>
                        <td>
                            <form action="{{URL::route('eliminar', $person->id)}} " method="POST">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button>Delete User</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection