@extends("layouts.app")

@section("content")

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif
                @endforeach
            </div> <!-- end .flash-message -->

            <button class="btn btn-info">Registrar persona</button>
            <a href="{{ URL::route('registro') }} ">Registro de persona</a>

            <a href="{{ URL::route('lista') }} ">Listado de personas</a>

        </div>
    </div>
</div>


@endsection