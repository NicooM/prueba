<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
//Route::get('listar',array('as' => 'lista', function () {
//    return view('listaPersona');
//}));

Route::get('/persona/lista', 'PersonController@index')->name('lista');
Route::get('/', 'HomeController@index')->name('inicio');

Route::get('/persona/registro', 'PersonController@create')->name('registro');
//Route::resource('person', 'PersonController',[
//    'names' => [
//        'create' => 'registro',
//        'store' => 'faq.new',
//        // etc...
//    ]
//]);
Route::post('/insert', 'PersonController@store');
Route::get('/persona/{id}/editar','PersonController@edit')->name('editar');
Route::post('/persona/actualizar','PersonController@update')->name('actualizar');
Route::delete('/persona/{id}','PersonController@destroy')->name('eliminar');

Route::get('/persona/profesor','Person_profesorController@index');
