<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{

    public function person(){
        return $this->belongsTo('App\Person','city_id');
    }

    protected $fillable = [
       'name'
    ];

    protected $hidden = [
        'id'
    ];
}
