<?php

namespace App;
use MongoDB\BSON\Timestamp;
use Illuminate\Database\Eloquent\Model;

class Profesor extends Model
{

    protected $fillable = [
        'name', 'surname'
    ];

    protected $hidden = [
        'id'
    ];

    protected $table = "profesores";

    public function people(){
        return $this->belongsToMany('App\Person','id')->withTimestamps();

    }

}
