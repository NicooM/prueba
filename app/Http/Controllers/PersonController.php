<?php

namespace App\Http\Controllers;

use App\Person;
use App\City;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class PersonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $people =  DB::table('people')
            ->join('cities', 'people.city_id', '=', 'cities.id')
            ->select('people.id','people.name as personName','cities.name as cityName','career','sex')->get();

        return view('person/index',compact('people'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $cities =  City::select('cities')->select('id','name')->get();
        return view('person/create',compact('cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|string|max:255',
            'city_id' => 'required|integer',
            'sex' => 'in:F,M'

        ]);

        $person = new Person($request->all());
        $person->career = (Input::has('career')) ? true : false;
        $person->city_id = $request->city_id;
        $person->save();



        $request->session()->flash('alert-success', 'El formulario ha sido enviado');
        return redirect('/')->with('status', 'Profile updated!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

//        $person = Person::find($id);
//        dd($person);
        $person = Person::where('id', $id)->first();

        $citySelected =  City::select('name')
            ->where('cities.id','=',$person->city_id)
            ->select('id','name')->first();

        $cities = City::select('cities')->select('id','name')->get();


        return view('person/edit',compact('person','cities','citySelected'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $person = Person::find($request->person_id);
        $person->name = $request->name;
        $person->city_id = $request->city;
        $person->career = (Input::has('career')) ? true : false;
        $person->sex = $request->sex;
        $person->save();

        return redirect('/')->with('status', 'Profile updated!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Person::destroy($request->id);
        $request->session()->flash('alert-success', 'La persona ha sido eliminada');
        return redirect('/');

    }
}
