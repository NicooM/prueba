<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{

    public function profesores(){
        return $this->belongsToMany('App\Profesor','people_profesores','people_id','profesor_id')->withTimestamps();

    }

    public function Cities(){
        return $this->hasOne('App\City','id');
    }
    protected $fillable = [
        'name', 'career','sex'
    ];

    protected $hidden = [
        'city_id'
    ];
}
