<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleProfesoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people_profesores', function(Blueprint $table)
        {
            $table->integer('people_id')->unsigned()->index();
            $table->foreign('people_id')->references('id')
                ->on('people')->onDelete('cascade');

            $table->integer('profesor_id')->unsigned()->index();
            $table->foreign('profesor_id')->references('id')
                ->on('profesores')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
